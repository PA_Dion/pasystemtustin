<!--- 
 Filename: application.cfc
 Created by: Focus Data Systems: Grant Theobald
 Modified on: 07/02/2016 
 Purpose: root application component for Progress Adviser scheduled system processes
 Please Note: 
--->

<cfcomponent output="false">

	<!---Production server--->
	<cfset this.name = "paSystem">
  	<cfset this.datasource = "pa_vivio">
  	<cfset this.errorHandling = false>
    
    <!---Testing server or local dev--->
	<!---<cfset this.name = "paSystem">--->
    <!---<cfset this.datasource = "pa_testing_vivio">--->
    <!---<cfset this.errorHandling = false>--->

<cfset this.clientmanagement=true>
<cfset this.sessionmanagement=true>
<cfset this.sessiontimeout="#createtimespan(0,8,0,0)#"><!--- (0,8,0,0) --->
<cfset this.applicationtimeout="#createtimespan(0,12,0,0)#"><!--- (30,8,0,0) --->
<cfset this.loginstorage="Session">
<cfset this.mappings = structNew() />
<cfset this.mappings["/cfc"] = getDirectoryFromPath(getCurrentTemplatePath()) & "cfc/" />
<cfset this.mappings["/root"] = getDirectoryFromPath(getCurrentTemplatePath()) & "/" />

<!--- Application Start Method --->
<cffunction name="onApplicationStart" returnType="boolean" output="false">
    <cfset APPLICATION.appStarted = now()>
    <cfset APPLICATION.serverTimeZone = "US/Pacific"><!---Serves as a visual only, no calcs are based on this value--->
    <cfset APPLICATION.adminTimeZoneOffset = createTime(0,0,0)*(-1)>
	<cfset APPLICATION.sessionCounter=0>
    <!--- <cfset APPLICATION.mailServer = "127.0.0.1"> --->
    <cfset APPLICATION.mailServer = "smtp.gmail.com">
    <!--- <cfset APPLICATION.mailUser = "root@progressadviser.com"> --->
    <cfset APPLICATION.mailUser = "no-reply@progressadviser.com">
    <!--- <cfset APPLICATION.mailPassword = "Rh32aa6f"> --->
    <cfset APPLICATION.mailPassword = "PA4educ!2">
    <cfset APPLICATION.email = "no-reply@progressadviser.com">
    <cfset APPLICATION.email2 = "support@progressadviser.com">
    <cfset APPLICATION.sysAdminEmail = "dev@progressadvsiser.com">
    <cfreturn true>
</cffunction>
  
  <!--- BEGIN Application End Method --->
  <cffunction name="onApplicationEnd" returnType="void" output="false">
    <cfargument name="appScope" required="true"> 

    <!--- Log how many minutes the application stayed alive --->
    <cflog file="#THIS.name#" text="App ended after #dateDiff('n',ARGUMENTS.appScope.appStarted,now())# minutes.">
  </cffunction>
  <!--- END Application End Method --->
  
  <!--- BEGIN Request Start Method --->
  <cffunction name="onRequestStart" returnType="boolean" output="true">
  
    <!---Use this to re-start the application on the next page request.--->
    <cfif NOT isDefined("APPLICATION.appStarted")>
	  <cfset applicationstop()/>
    </cfif>
  
     <!--- Define the local scope. --->
		<cfset var local = {} />
    
	<!--- Return true so the page can process. --->
    <cfreturn true />
  
  </cffunction>
  <!--- END Request Start Method --->
  
  <!--- BEGIN On Error Method --->
  <cffunction name="onError">

    <!--- Define arguments.--->
    <cfargument
    name="exception"
    type="any"
    required="true"
    hint="I am the uncaught exception object."
    />
     
    <cfargument
    name="event"
    type="string"
    required="false"
    default=""
    hint="I am the event handler in which the error occurred."
    />
     
    <!--- Define the local scope. --->
    <cfset var local = {} />
     
    <!---
    First things first, let's set the page timeout high
    in case we are recoverring from a timeout issue on
    the page. This will give us time to recover.
    --->
    <cfsetting requesttimeout="#(5 * 60)#" />
     
     
    <!---
    Build the collection of scopes we want to output
    in our debuggin report.
    --->
    <cfset local.scopes = [
    {
    name = "Exception",
    data = arguments.exception
    },
    {
    name = "Form",
    data = form
    },
    {
    name = "Url",
    data = url
    },
    {
    name = "CGI",
    data = cgi
    }
    ] />
     
    <!---
    Now that we have time to operate our error recovery,
    let's build a report of th error.
    --->
    <cfsavecontent variable="local.report">
     
    <h1>
    An Error Occurred
    </h1>
     
    <!---
    Loop over the given scopes to output each one
    for debugging purposes.
    --->
    <cfloop
    index="local.scope"
    array="#local.scopes#">
     
     <cfoutput>
    <h2>
    <cfoutput>#UCase( local.scope.name )#</cfoutput> Data
    </h2>
    </cfoutput>
     
    <cfdump
    var="#local.scope.data#"
    label="#local.scope.name# Data"
    top="5"
    />
     
    </cfloop>
     
    </cfsavecontent>
     
     
    <!---BEGIN Don't mail error for local and testing--->
	<cfif this.errorHandling>

    <!---
    Mail out the report. Attach the primary report to
    the email so that GMail does not block it as spam.
    Put only minial debug information in the mail body.
    --->

    <cfmail to="#APPLICATION.sysAdminEmail#" 
      from="errors@progressadviser.com" subject="ERROR: #APPLICATION.URL# : #arguments.exception.message#" type="html" 
      server="#APPLICATION.mailServer#" username="#APPLICATION.mailUser#" password="#APPLICATION.mailPassword#">
     
    <h1>
    An Error Occurred on ProgressAdviser.com
    </h1>

    <h2>User: #SESSION.fullName#<br>
    District: #SESSION.districtName#<br>
    School: #SESSION.schoolName#</h2>

    <h2>Browser and Page</h2>
    Browser: #CGI.HTTP_USER_AGENT#<br>
    Referring Page: #CGI.HTTP_REFERER#<br>
    URL Vars: #CGI.QUERY_STRING#<br>
     
    <h2>
    Message
    </h2>
     
    <p>
    <cfif len( arguments.exception.message )>
    #arguments.exception.message#
    <cfelse>
    <em>No message</em>
    </cfif>
    </p>
     
    <h2>
    Details
    </h2>
     
    <p>
    <cfif len( arguments.exception.detail )>
    #arguments.exception.detail#
    <cfelse>
    <em>No details</em>
    </cfif>
    </p>
     
    <!--- Check to see if there is a tag context. --->
    <cfif arrayLen( arguments.exception.tagContext )>
     
    <!---
    Get a shorthand reference to the top tag
    context for easy output.
    --->
    <cfset local.context = arguments.exception.tagContext[ 1 ] />
     
    <h2>
    Tag Context
    </h2>
     
    <dl>
    <cfloop
    item="local.contextKey"
    collection="#local.context#">
     
    <dt>
    #local.contextKey#
    </dt>
    <dd>
    #local.context[ local.contextKey ]#
    </dd>
     
    </cfloop>
    </dl>
     
    </cfif>
     
     
    <!---
    Attach our full debugging report to the email so
    that GMail does not block the raw data as SPAM.
    --->
    <cfmailparam
    content="#local.report#"
    file="error_report.htm"
    />
     
    </cfmail>
    
    <!--- Redirect to error page. --->
    <cflocation
    url="appOnError.cfm"
    addtoken="false"
    />
    
    <cftry>
    Catch relocation error. 
    <cfcatch>
     
    <!--- Use javascript. --->
    <script type="text/javascript">
    location.href="appOnError.cfm";
    </script>
     
    </cfcatch>
    </cftry>
    
    <!---Otherwise show the error report here--->
    <cfelse>
    
    <cfoutput>#local.report#</cfoutput>
    
    </cfif>
	<!---END Don't mail error for local and testing--->
     
    <!--- Return out. --->
    <cfreturn />
  
  </cffunction>
  <!--- END On Error Method --->
  
</cfcomponent>
<!--- 
 Filename: ad_1.cfm
 Created by: Focus Data Systems, Grant Theobald
 Created on: 2016-06-01
 Purpose: Process user date for auto-import from csv file.
 Modified on: 2016-10-22 to add file mgt and updates in processing
 Modifications: 
 Please Note: 
--->

<!---<cfsetting requestTimeout="3600" />--->

<!---Currently used for Tustin only--->
<cfset this.districtID = 10209>
<cfparam name="this.runLocal" default="false">
<!---<cfset this.runLocal = true>--->

<cfif this.runLocal>

	<cfset this.csvFileName = 'exportedusers_170903200628.csv'>

<cfelse>

<!--- Upload CSV-TXT file --->
<cfset thisPath = ExpandPath("*.*")>
<!---<cfset csvFilePath = GetDirectoryFromPath(thisPath)&'dataFiles/'>--->
<!---<cfset csvFilePath = GetDirectoryFromPath(thisPath)&'mappedFiles/'>--->
<cfset csvFilePath = 'c:\mapped\progressAdviser\tustin\'>

<cfset this.fileList = DirectoryList(csvFilePath, false, 'query')>

<!---<cfdump var="#this.fileList#">--->

<cfquery name="getUserFiles" dbtype="query">
SELECT		name
FROM 		this.fileList
WHERE		name like 'exportedusers_%'
</cfquery>

<cfset qryFileDates = QueryNew("name, fileDate")>

<!---The last 12 chars of the filename gives the date / timestamp--->
<cfloop query="getUserFiles">
	<cfset this.fileDate = mid(getUserFiles.name,15,12)>
	<cfset temp = QueryAddRow(qryFileDates)>
    <cfset temp = QuerySetCell(qryFileDates, "name", getUserFiles.name)> 
    <cfset temp = QuerySetCell(qryFileDates, "fileDate", this.fileDate)> 
</cfloop>

<!---<cfdump var="#qryFileDates#">--->

<cfquery name="getID" dbtype="query" maxrows="1">
SELECT		max(fileDate) AS maxFileDate
FROM 		qryFileDates
</cfquery>

<!---<cfdump var="#getID#">--->

<cfquery name="getFile" dbtype="query" maxrows="1">
SELECT	name
FROM		getUserFiles
WHERE	name like '%#getID.maxFileDate#%'
</cfquery>

<cfset this.csvFileName = getFile.name>

<cfset this.fileSource = #csvFilePath# & #this.csvFileName#>

<cfset this.fileDestination = 'c:\inetpub\wwwroot\paSystem\dataFiles\' & #this.csvFileName#>

<cfoutput>this.fileSource: #this.fileSource#<br>this.fileDestination: #this.fileDestination#</cfoutput>
    
<cffile  
    action = "copy"  
    source = "#this.fileSource#"  
    destination = "#this.fileDestination#"
    nameconflict="overwrite">

</cfif>
<!--- END run local --->

<!---<cfoutput>this.csvFileName: #this.csvFileName#</cfoutput>line 81<cfabort>--->

<!---<cfset dataUrl = (--->
    <!---"http://" &--->
    <!---cgi.server_name &--->
    <!---getDirectoryFromPath( cgi.script_name ) & 'dataFiles/' &--->
    <!---this.csvFileName--->
    <!---) />--->

<cfset dataUrl = (
    "http://" & cgi.server_name &
    '/paSystem/dataFiles/' &
    this.csvFileName
    ) />
    <br>
    <cfoutput>
        dataUrl: #dataUrl#
    </cfoutput>
    <br>
    
    <cfhttp
    name="qryUsers"
    method="get"
    url="#dataUrl#"
    columns="displayName, logonName, firstName, lastName, email, category, department, title, distinguishedName"
    />

    <cfquery name="getUsers" dbtype="query" timeout="0">
    SELECT		*
    FROM			qryUsers
    WHERE		displayName NOT LIKE ''
    					AND logonName NOT LIKE ''
    					AND firstName NOT LIKE ''
    					AND lastName NOT LIKE ''
    					AND email NOT LIKE ''
    					AND category NOT LIKE ''
    					<!---AND department NOT LIKE ''--->
    					AND title NOT LIKE ''
    					AND distinguishedName NOT LIKE ''
                        AND department NOT LIKE 'Student'
    </cfquery>
    
   <!--- <cfdump var="#getUsers#">line 114<cfabort>--->
    
    <!---BEGIN Check for new titles--->
    
	<!---Initialize the send mail flag--->
    <cfset vSendEmail = 0>
    
    <cfquery name="qryTitles" dbtype="query">
    SELECT 		COUNT([lastName]) AS userCount, title
    FROM				getUsers
    WHERE			title NOT LIKE '_Not Set'
    GROUP BY	title
    ORDER BY	title
    </cfquery>
    
    <!---<cfdump var="#qryTitles#"><cfabort>--->
    
    <cfloop query="qryTitles">
    
    	<cfset this.title = trim(qryTitles.title)>
    
    	<cfquery name="checkTitle">
        SELECT	 	id, title, userType, userType2
        FROM		 	tblUserTypes_mapping
        WHERE		districtID = <cfqueryparam cfsqltype="cf_sql_integer" value="#this.districtID#">
        					AND title LIKE '#this.title#'
        </cfquery>
        
        <cfif NOT checkTitle.recordCount>
            <cfquery>
            insert into tblUserTypes_mapping ( [districtID], [title], [userType], [userType2] ) 
            values (#this.districtID#, '#this.title#',0,0)
            </cfquery>
            
            <cfset vSendEmail = 1>
            
            <!---<cfoutput>#this.districtID#, '#this.title#',0,0</cfoutput><br/>--->
    	</cfif>
    </cfloop>
    
    <cfif vSendEmail>
    	
		<cfquery name="getNewTitles">
            SELECT	 	id, title, userType, userType2
            FROM		 	tblUserTypes_mapping
            WHERE		userType = 0
        </cfquery>

        <br> New user titles: <cfdump var="#getNewTitles#"> <br>
        
    <!---Send email to todd with new titles--->
    <cfmail to="#APPLICATION.sysAdminEmail#" 
      from="system@progressadviser.com" subject="LDAP Import - New User Types" type="html" 
      server="#APPLICATION.mailServer#" username="#APPLICATION.mailUser#" password="#APPLICATION.mailPassword#">
      <p>PA Admin,
      
      There are new user types that need mapping.  Use this query to pull them up:
      
      	select * from tblUserTypes_mapping
		where userType = 0
		order by id
      </p>

      </cfmail>
      
    </cfif>
    
    <!---END Check for new titles--->

    <cfinclude template="ad_2.cfm">
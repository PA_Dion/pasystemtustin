<!--- 
 Filename: ad_2.cfm
 Created by: Focus Data Systems, Grant Theobald
 Created on: 2016-06-01
 Purpose: Process user date for auto-import from csv file.
 Modified on: 2019-06-20
 Modifications: re-added de-activation of users, fixed user updates/addition of new users
 Please Note: included by ad_1.cfm
--->

<!---Currently used for Tustin only--->
<cfset this.districtID = 10209>
<cfset this.districtGroupID = 10127>

<cfquery>DELETE FROM tblUsers_temp</cfquery>

<cfloop query="getUsers">
    <cfset this.dn = getUsers.distinguishedName>
    <cfif this.dn contains('OU=')>
<!---Parse dn for the location, this will currently work only for "tustin", we will need to evaluate the length of that string to apply to other values/districts--->
<!---<cfset this.locationName = trim(reverse(left(reverse(left(this.dn,len(this.dn) - 17)), find('=UO', reverse(left(this.dn,len(this.dn) - 17)))-1)))>--->
        <cfset this.locationName = "">
        <cfloop list="#this.dn#" index="name">
            <cfset x = #trim(name)#>
<!---<br>x: <cfdump var="#x#">--->
            <cfif (x CONTAINS "OU=")
            AND (NOT x CONTAINS "Employee")
            AND (NOT x CONTAINS "Student")
            AND (NOT x CONTAINS "User")
            AND (NOT x CONTAINS "Special Education")>
                <cfset this.locationName = Trim(Replace(x, "OU=", ""))>
<!---<br>locationName: <cfdump var="#this.locationName#">--->
                <cfbreak>
            </cfif>
        </cfloop>

        <cflock timeout="0">
            <cfquery timeout="0">
            INSERT INTO tblUsers_temp (logonName, firstName, lastName, email, category, title, distinguishedName, locationName)
            VALUES		('#getUsers.logonName#', '#getUsers.firstName#', '#getUsers.lastName#', '#getUsers.email#', '#getUsers.category#', '#getUsers.title#', '#getUsers.distinguishedName#', '#this.locationName#')
            </cfquery>
        </cflock>
    </cfif>
</cfloop>

<p>ad_2.cfm Line 32: Import OK</p>

<cfquery name="getLocationsTemp">
   SELECT  DISTINCT u.locationName
   FROM		tblUsers_temp u
   ORDER BY	 u.locationName
   </cfquery>

<cfdump var="#getLocationsTemp#">
<!---<cfabort>--->

<cfquery name="getUsersTemp">
   SELECT  u.logonName, u.firstName, u.lastName, u.email, u.category, u.title, u.locationName, u.distinguishedName
   FROM		tblUsers_temp u 
   ORDER BY	 u.lastName, u.firstName
   </cfquery>

Temp Users BEFORE Check:
<cfoutput>#getUsersTemp.recordCount#</cfoutput><br><br>

line 48
<!---<cfabort>--->

<!---BEGIN Check for and de-activate existing PA users that are not in the temp table--->

<cfquery name="getExistingUsers">
   	select 		userKey, ltrim(rtrim(schoolName)) as schoolName, firstName, lastName, userLogin, userEmail
    from 		vUsersSchools
	where 		districtID = #this.districtID#
    --and 			userPassword is null
    and			inactive = 0

    <cfif this.runLocal>
        and userKey = 42721 <!---temp account--->
    </cfif>
</cfquery>

Tustin Active Users BEFORE Inactive check:
<cfoutput>#getExistingUsers.recordCount#</cfoutput><br><br>

<cfif getExistingUsers.recordCount>
    <cfset i = 0>
    <cfloop query="getExistingUsers">

<!---Check for this user in the temp table--->
        <cfquery name="checkUserTemp" dbtype="query" maxrows="1">
        select 		logonName
        from			getUsersTemp
        where 	 	logonName like '#getExistingUsers.userLogin#'
        <!---and			firstName like '#getExistingUsers.firstName#'--->
        <!---and			lastName like '#getExistingUsers.lastName#'--->
        <!---and			email like '#getExistingUsers.userEmail#'--->
        <!---and 			(locationName like '#getExistingUsers.schoolName#' --->
                    	<!---or (locationName LIKE 'District%' and '#getExistingUsers.schoolName#' LIKE 'District%'))--->
        </cfquery>

        <cfif NOT checkUserTemp.recordCount>
            <cfquery>
                UPDATE	tblUsers
                SET				inactive = 1
                WHERE		userKey = #getExistingUsers.userKey#
            </cfquery>
            <cfoutput>Updated user to inactive  - #i# - id: #getExistingUsers.userKey# login: #getExistingUsers.userLogin# email: #getExistingUsers.userEmail# name: #getExistingUsers.lastName#, #getExistingUsers.firstName#</cfoutput>
            <br>
            <cfset i++>
        </cfif>

    </cfloop>

    <cfif i GTE 1>
        <p><cfoutput>#i# user(s) set to inactive</cfoutput></p>
    <cfelse>
        <p><cfoutput>0 users set to inactive</cfoutput></p>
    </cfif>

</cfif>

<!---END Check for and de-activate existing PA users that are not in the temp table--->


<!---BEGIN Check for and re-activate existing PA users that ARE in the temp table--->

<cfquery name="getExistingInactiveUsers">
   	select 		userKey, ltrim(rtrim(schoolName)) as schoolName, firstName, lastName, userLogin, userEmail
    from 		vUsersSchools
	where 		districtID = #this.districtID#
    and			inactive = 1

    <cfif this.runLocal>
    and userKey = 42721 <!---temp account--->
</cfif>
</cfquery>

Tustin Inactive Users BEFORE Inactive check:
<cfoutput>#getExistingInactiveUsers.recordCount#</cfoutput><br><br>

<cfif getExistingInactiveUsers.recordCount>
    <cfset i = 0>
    <cfloop query="getExistingInactiveUsers">

<!---Check for this user in the temp table--->
        <cfquery name="checkUserTemp" dbtype="query" maxrows="1">
        select 		logonName
        from			getUsersTemp
        where 	 	logonName like '#getExistingInactiveUsers.userLogin#'
        </cfquery>

        <cfif checkUserTemp.recordCount>
            <cfquery>
                UPDATE	tblUsers
                SET				inactive = 0
                WHERE		userKey = #getExistingInactiveUsers.userKey#
            </cfquery>
            <cfoutput>Updated user to active  - #i# - id: #getExistingInactiveUsers.userKey# login: #getExistingInactiveUsers.userLogin# email: #getExistingInactiveUsers.userEmail# name: #getExistingInactiveUsers.lastName#, #getExistingInactiveUsers.firstName#</cfoutput>
            <br>
            <cfset i++>
        </cfif>

    </cfloop>

    <cfif i GTE 1>
        <p><cfoutput>#i# user(s) set to active</cfoutput></p>
    <cfelse>
        <p><cfoutput>0 users set to active</cfoutput></p>
    </cfif>

</cfif>

<!---END Check for and re-activate existing PA users that are in the temp table--->


<!---BEGIN Check for and delete existing/unchanged users from the temp table--->
<cfquery name="checkUsers">

   SELECT
   u.userLogin as logonName, u.firstName as userTableFirstName, u.lastName as userTableLastName, u.userEmail, u.userType, u.userType2,
   ut.email, ut.title, ut.firstName, ut.lastName,
   t.title as userTypesTitle, t.userType as userTypesUserType, t.userType2 as userTypesUserType2
   FROM		tblUsers_temp ut
   join vUsersSchools u on ut.logonName = u.userLogin
   join tblUserTypes_mapping t on ltrim(rtrim(ut.title)) = ltrim(rtrim(t.title))
<!---and ut.firstName = u.firstName --->
<!---and ut.lastName = u.lastName--->
<!---and ut.email = u.userEmail--->
   and (ltrim(rtrim(ut.locationName)) = ltrim(rtrim(u.schoolName))
   or ( ltrim(rtrim(ut.locationName)) LIKE 'District%' and  ltrim(rtrim(u.schoolName)) LIKE 'District%' ))
   ORDER BY	 u.lastName, u.firstName
   </cfquery>

<br>
CheckUsers (existing users in both the extract and tblUsers):
<cfoutput>#checkUsers.recordCount#</cfoutput><br><br>
<cfdump var="#checkUsers#">
<!---<cfabort>--->
<br>

<cfif checkUsers.recordCount>

    <cfloop query="checkUsers">
        <cfset this.logonName = checkUsers.logonName>
        <cfset this.firstName = checkUsers.firstName>
        <cfset this.lastName = checkUsers.lastName>
        <cfset this.email = checkUsers.email>

        <cfset this.title = checkUsers.title>
        <cfset this.userType = checkUsers.userType>
        <cfset this.userType2 = checkUsers.userType2>
        <cfset this.userTypesTitle = checkUsers.userTypesTitle>

        <cfif   checkUsers.userEmail EQ '#this.email#' AND
                checkUsers.userTableFirstName EQ '#this.firstName#' AND
                checkUsers.userTableLastName EQ '#this.lastName#' AND
                checkUsers.title EQ '#this.userTypesTitle#' AND
                checkUsers.userTypesUserType EQ '#this.userType#' AND
                checkUsers.userTypesUserType2 EQ '#this.userType2#'
                >
        <cfquery>
           DELETE FROM tblUsers_temp
           WHERE	logonName = '#this.logonName#'
                    <!---and firstName = '#this.firstName#'--->
                    <!---and lastName = '#this.lastName#'--->
                    <!---and email = '#this.email#'--->
                    <!---and title = '#this.userTypesTitle#'--->
        </cfquery>
        <br>
        Delete User from tblUsers_temp because it hasn't changed:
        <cfoutput>#this.logonName# #this.email# #this.firstName# #this.lastName# #this.userTypesTitle#</cfoutput>
        </cfif>
<!---<cfabort>--->
    </cfloop>
</cfif>

<cfquery name="getUsersTemp_2">
   SELECT  u.logonName, u.firstName, u.lastName, u.email, u.category, u.title, u.locationName, u.distinguishedName,
           t.title as userTypesTitle, t.userType, t.userType2
   FROM		tblUsers_temp u
   join tblUserTypes_mapping t on ltrim(rtrim(u.title)) = ltrim(rtrim(t.title))
   ORDER BY	 u.lastName, u.firstName
   </cfquery>

<br><br>Temp Users AFTER Delete of users that haven't changed:
<cfoutput>#getUsersTemp_2.recordCount#</cfoutput><br><br>
<cfdump var="#getUsersTemp_2#">
<!---END Check for and delete existing users from the temp table--->

<!---BEGIN Update the remaining records, then delete all temp records that match on userLogin--->
<cfif getUsersTemp_2.recordCount>
    <cfset i = 1>
    <cfloop query="getUsersTemp_2">

        <cfset this.logonName = getUsersTemp_2.logonName>

        <cfquery name="getUser" maxrows="1">
        select	userKey, userEmail,userLogin,firstName,lastName,userType,userType2
        from		tblUsers
        where	userLogin LIKE '#this.logonName#'
        </cfquery>

<!---BEGIN IF user match, then update and delete from temp users--->
        <cfif getUser.recordCount>

            <cfset this.userKey = getUser.userKey>
            <cfset this.email = getUsersTemp_2.email>
            <cfset this.firstName = getUsersTemp_2.firstName>
            <cfset this.lastName = getUsersTemp_2.lastName>
            <cfset this.locationName = getUsersTemp_2.locationName>

            <cfset this.title = getUsersTemp_2.title>
            <cfset this.userType = getUsersTemp_2.userType>
            <cfset this.userType2 = getUsersTemp_2.userType2>
            <cfset this.userTypesTitle = getUsersTemp_2.userTypesTitle>

<!--- BEGIN Get the schoolID --->
            <cfif this.locationName CONTAINS 'District'>
                <cfset this.schoolID = 0>
            <cfelse>

                <cfquery name="getSchool" maxrows="1">
                select id from tblSchools s
                where  districtID = #this.districtID#
                			and ltrim(rtrim(s.name)) like  ltrim(rtrim('#this.locationName#'))
                </cfquery>
                <cfset this.schoolID = getSchool.id>

            </cfif>
<!--- END Get the schoolID --->

            <!---<cfoutput>this.schoolID: #this.schoolID#</cfoutput>--->

            <cfif   getUser.userEmail NEQ '#this.email#' OR
                    getUser.firstName NEQ '#this.firstName#' OR
                    getUser.lastName NEQ '#this.lastName#' OR
                    getUser.userType NEQ '#this.userType#' OR
                    getUser.userType2 NEQ '#this.userType2#'
                    >

                <cfoutput><br>Update changed user:
                    #this.lastName#, #this.firstName# email: #this.email# this.schoolID: #this.schoolID# #this.logonName# #this.userTypesTitle#
                </cfoutput>

                <cfquery>
                update tblUsers
                set
                userEmail = '#this.email#',
                <!---userLogin = '#this.logonName#',--->
                firstName = '#this.firstName#',
                lastName = '#this.lastName#'
                <cfif this.userType GT 0>
                  ,userType = '#this.userType#'
                </cfif>
                <cfif this.userType2 GT 0>
                  ,userType2 = '#this.userType2#'
                </cfif>
                where userKey = <cfqueryparam cfsqltype="cf_sql_integer" value="#this.userKey#">
                </cfquery>
            <cfelse>
<!---No change--->
            </cfif>

<!---Check for single location--->
            <cfquery name="getLocation">
            select schoolID
            from tblUsersSchools
            where userKey =
                <cfqueryparam cfsqltype="cf_sql_integer" value="#this.userKey#">
            </cfquery>

<!---BEGIN update only if they have a single location--->
            <cfif getLocation.recordCount EQ 1 and isDefined('this.schoolID') and this.schoolID NEQ ''>

                <cfif  getLocation.schoolID NEQ this.schoolID>
                <cfoutput><br>Single location getLocation.schoolID: #getLocation.schoolID# this.schoolID: #this.schoolID#</cfoutput>
                <cfquery>
                  update tblUsersSchools
                  set schoolID = #this.schoolID#
                  where userKey =
                    <cfqueryparam cfsqltype="cf_sql_integer" value="#this.userKey#">
                </cfquery>
                </cfif>

             <cfelseif getLocation.recordCount GT 1>

                <cfquery name="getUserLocations">
                  SELECT		districtGroupID, districtID, schoolID,unionID, userKey, subKey
                  FROM 			tblUsersSchools
                  WHERE 		userKey = <cfqueryparam cfsqltype="cf_sql_integer" value="#this.userKey#">
                </cfquery>

                <cfset schoolIDs = valueList(getUserLocations.schoolID)>
                <cfif  ListFind(schoolIDs,this.schoolID) EQ 0>
                    <cfloop query="getUserLocations">

                            <cfquery>
                                INSERT INTO 	tblUsersSchools (unionID, userKey, districtGroupID, districtID, schoolID, subKey)
                                VALUES				(#getUserLocations.unionID#,
                                                    #getUserLocations.userKey#,
                                                    #getUserLocations.districtGroupID#,
                                                    #getUserLocations.districtID#,
                                                    #this.schoolID#,
                                                    0)
                            </cfquery>

                        <cfoutput>Multiple locations schoolIDs: #schoolIDs#</cfoutput>
                        <cfoutput><br>Added new location: #this.schoolID#</cfoutput>

                        <cfbreak>
                    </cfloop>
                </cfif>
            </cfif>
<!---END update only if they have a single location--->

            <cfquery>
               DELETE FROM tblUsers_temp
               WHERE	logonName LIKE '#this.logonName#'
                                and firstName LIKE '#this.firstName#'
                                and lastName LIKE '#this.lastName#'
                                and email LIKE '#this.email#'
            </cfquery>
            <br>
            <cfoutput>Deleted from tblUsers_temp after update - #i# - id: #this.userKey# email: #this.email# locationName: #this.locationName# schoolID: #this.schoolID# userType: #this.userType# userType2:#this.userType#</cfoutput>
            <cfset i++>

        </cfif>
<!---BEGIN IF user match--->

    </cfloop>

</cfif>
<!--- END Update the remaining records --->

<cfquery name="getUsersTemp_3">
   SELECT  u.logonName, u.firstName, u.lastName, u.email, u.category, u.title, u.locationName, u.distinguishedName
   FROM		tblUsers_temp u 
   ORDER BY	 u.lastName, u.firstName
   </cfquery>

<br>
Temp Users AFTER Update:
<cfoutput>#getUsersTemp_3.recordCount#</cfoutput><br><br>
<cfdump var="#getUsersTemp_3#">
<!---<cfabort>--->
<br>

<!---BEGIN Create new user accounts --->
<cfif getUsersTemp_3.recordCount>

<!---BEGIN query and insert District users (schoolID = 0)--->
    <cfquery name="d">
   SELECT  u.logonName, u.firstName, u.lastName, u.email, u.category, u.title, u.locationName,
           t.userType, t.userType2, 0 as schoolID
   FROM		tblUsers_temp u
            join tblUserTypes_mapping t on ltrim(rtrim(u.title)) = ltrim(rtrim(t.title))
   WHERE 	u.locationName LIKE 'District%'
   					AND t.userType <> 0
   ORDER BY	 u.lastName, u.firstName
   </cfquery>

    <cfif d.recordCount>

        <cfloop query="d">

            <cfset vRandom = dateformat(now(), 'mmddyy') & timeformat(now(), 'mmss') & randrange(10, 99)>

            <cfquery result="vResult">
            INSERT INTO tblUsers (userLogin, firstName, lastName, userEmail, userType, userType2)
            VALUES ('#d.logonName#', '#d.firstName#', '#d.lastName#', '#d.email#', #d.userType#, #d.userType2#)
            </cfquery>

            <cfset this.userKey = vResult["GENERATEDKEY"]>

            <cfquery>
            INSERT INTO 	tblUsersSchools (unionID, userKey, districtGroupID, districtID, schoolID, subKey)
            VALUES				(rtrim(cast(#this.userKey# AS CHAR)) + rtrim(cast(#vRandom# AS CHAR)), 
                                        #this.userKey#,#this.districtGroupID#, #this.districtID#, #d.schoolID#, 0)
            </cfquery>

        </cfloop>

    </cfif>
<!---END query and insert District users--->

<!---BEGIN query and insert School level users--->
    <cfquery name="s">
   SELECT  u.logonName, u.firstName, u.lastName, u.email, u.category, u.title, u.locationName, t.userType, t.userType2, s.ID as schoolID
   FROM		tblUsers_temp u join tblUserTypes_mapping t on ltrim(rtrim(u.title)) = ltrim(rtrim(t.title)) join tblSchools s on ltrim(rtrim(u.locationName)) = ltrim(rtrim(s.name))
   WHERE 	u.locationName NOT LIKE 'District%'
   					AND t.userType <> 0
                    AND s.districtID =
        <cfqueryparam cfsqltype="cf_sql_integer" value="#this.districtID#">
   ORDER BY	 u.lastName, u.firstName
   </cfquery>

    School New Users:
    <cfdump var="#s#">
    <cfif s.recordCount>

        <cfloop query="s">

            <cfset vRandom = dateformat(now(), 'mmddyy') & timeformat(now(), 'mmss') & randrange(10, 99)>

            <cfquery result="vResult">
            INSERT INTO tblUsers (userLogin, firstName, lastName, userEmail, userType, userType2)
            VALUES ('#s.logonName#', '#s.firstName#', '#s.lastName#', '#s.email#', #s.userType#, #s.userType2#)
            </cfquery>

            <cfset this.userKey = vResult["GENERATEDKEY"]>

            <cfquery>
            INSERT INTO 	tblUsersSchools (unionID, userKey, districtGroupID, districtID, schoolID, subKey)
            VALUES				(rtrim(cast(#this.userKey# AS CHAR)) + rtrim(cast(#vRandom# AS CHAR)), 
                                        #this.userKey#,#this.districtGroupID#, #this.districtID#, #s.schoolID#, 0)
            </cfquery>

        </cfloop>

    </cfif>
<!---END query and insert School level users--->

    <cfoutput>
        getUsers.recordCount: #getUsers.recordCount#<br>
    d.recordCount: #d.recordCount#<br>
    s.recordCount: #s.recordCount#<br>
    </cfoutput>

</cfif>
<!---END Create new user accounts --->

<!---BEGIN reactivate PA user --->
<cfquery>
UPDATE tblUsers SET inactive = 0 WHERE userKey = 39354
</cfquery>
<!---END reactivate PA user --->

<!---Clear the temp table--->
<cfquery>DELETE FROM tblUsers_temp</cfquery>

<!---BEGIN only for production--->
<cfif NOT this.runLocal>

    <cfset this.fileSource = this.fileDestination>
    <cfset this.fileDestination = 'c:\inetpub\wwwroot\paSystem\dataFiles\processed\' & #this.csvFileName#>

    <cffile
            action="move"
            source="#this.fileSource#"
            destination="#this.fileDestination#"
            nameconflict="makeunique">

<!--- Log every run --->
    <cflog text="File: #this.csvFileName#, District ID: #this.districtID#, success" application="yes" file="paSystemLog">

<cfelse>
    <p> Local Run </p>
</cfif>
<!---END only for production--->